#!/bin/bash
set -x
(composer phpunit:entrypoint -- --testsuite split_group_7 --group \
	Database --exclude-group Broken,ParserFuzz,Stub,Standalone 2>&1 \
	| tee test_run_7.log ; test ${PIPESTATUS[0]} -eq 0) & \
(composer phpunit:entrypoint -- --testsuite split_group_6 --group \
	Database --exclude-group Broken,ParserFuzz,Stub,Standalone 2>&1 \
	| tee test_run_6.log ; test ${PIPESTATUS[0]} -eq 0) & \
(composer phpunit:entrypoint -- --testsuite split_group_5 --group \
	Database --exclude-group Broken,ParserFuzz,Stub,Standalone 2>&1 \
	| tee test_run_5.log ; test ${PIPESTATUS[0]} -eq 0) & \
(composer phpunit:entrypoint -- --testsuite split_group_4 --group \
	Database --exclude-group Broken,ParserFuzz,Stub,Standalone 2>&1 \
	| tee test_run_4.log ; test ${PIPESTATUS[0]} -eq 0) & \
(composer phpunit:entrypoint -- --testsuite split_group_3 --group \
	Database --exclude-group Broken,ParserFuzz,Stub,Standalone 2>&1 \
	| tee test_run_3.log ; test ${PIPESTATUS[0]} -eq 0) & \
(composer phpunit:entrypoint -- --testsuite split_group_2 --group \
	Database --exclude-group Broken,ParserFuzz,Stub,Standalone 2>&1 \
	| tee test_run_2.log ; test ${PIPESTATUS[0]} -eq 0) & \
(composer phpunit:entrypoint -- --testsuite split_group_1 --group \
	Database --exclude-group Broken,ParserFuzz,Stub,Standalone 2>&1 \
	| tee test_run_1.log ; test ${PIPESTATUS[0]} -eq 0) & \
(composer phpunit:entrypoint -- --testsuite split_group_0 --group \
	Database --exclude-group Broken,ParserFuzz,Stub,Standalone 2>&1 \
	| tee test_run_0.log ; test ${PIPESTATUS[0]} -eq 0) &
FAIL=0
for job in `jobs -p`; do
  echo "Waiting for $job..."
  wait $job || let "FAIL+=1"
done
for i in `seq 0 7`; do
  echo "LOG FOR split_group_${i}"
  cat test_run_${i}.log
done
echo "$FAIL suites failed"
if [ "$FAIL" == "0" ]; then
  echo "YAY!"
else
  echo "FAIL! ($FAIL)"
fi
exit $FAIL
