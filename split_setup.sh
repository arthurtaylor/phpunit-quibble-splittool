#!/bin/bash
set -ex
ROOT=`dirname $0`
composer phpunit:entrypoint -- --testsuite extensions --list-tests-xml test-cases-integration.xml
if [ -f ${ROOT}/phpunit-results.cache ]; then
  python3 ${ROOT}/split.py test-cases-integration.xml 7 ${ROOT}/phpunit-results.cache
else
  python3 ${ROOT}/split.py test-cases-integration.xml 7
fi

