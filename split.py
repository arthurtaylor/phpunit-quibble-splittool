#!/usr/bin/env python3
import os
import json
import sys
import re
from xml.etree import ElementTree as ET
from xml.dom.minidom import parseString
from pathlib import Path

class ResultCacheParser:

    def parse_result_cache(self, cache_file):
        with open(cache_file, 'r') as file:
            cache_data = file.read()
        cache_array = json.loads(cache_data)
        times = cache_array['times']
        time_spent_per_class = {}
        for test_case, time_spent in times.items():
            class_name = test_case.split('::')[0]
            time_spent_per_class[class_name] = time_spent_per_class.get(class_name, 0) + time_spent
        return time_spent_per_class

class TestListParser:

    def extract_namespace(self, test_case_class):
        parts = test_case_class.split('\\')
        class_name = parts.pop()
        return parts, class_name

    def parse_test_list(self, test_list_file, timing_data):
        tests_xml = ET.parse(test_list_file)
        tests = []
        for test_case_class in tests_xml.findall('testCaseClass'):
            test_name = test_case_class.get("name")
            namespace, class_name = self.extract_namespace(test_name)
            last_run_time = timing_data.get(test_name, None)
            tests.append((class_name, namespace, last_run_time))
        return tests

class FilesystemScanner:

    def scan_directory_for_php_files(self, directory):
        php_files = {}
        ignore_list = ["vendor", "ParserIntegrationTest.php"]
        for root, dirs, files in os.walk(directory):
            # Filter out unwanted directories
            dirs[:] = [d for d in dirs if d not in ignore_list]
            for file in files:
                if file.endswith('.php') and file not in ignore_list:
                    filename = os.path.basename(file)
                    if filename not in php_files:
                        php_files[filename] = []
                    php_files[filename].append(os.path.join(root, file))
        return php_files

    def extract_namespace_from_file(self, filename):
        with open(filename, 'r') as file:
            contents = file.read()
        matches = re.search(r'\bnamespace\s+([^\s;]+)', contents)
        if matches:
            return matches.group(1).split('\\')
        return []

    def resolve_correct_file(self, test_class, namespace_array, php_files):
        filename = test_class + ".php"
        if filename not in php_files:
            print("No same-named file found for class", filename)
            return None
        if len(php_files[filename]) == 1:
            return php_files[filename][0]
        for file in php_files[filename]:
            namespace = self.extract_namespace_from_file(file)
            if namespace == namespace_array:
                return file
        print("No namespace match found for", test_class)
        return None

    def find_test_files(self, tests):
        php_files = self.scan_directory_for_php_files(".")
        test_files = []
        for test in tests:
            test_files.append((self.resolve_correct_file(test[0], test[1], php_files), test[2]))
        return test_files

class SuiteBuilder:

    def smallest_group(self, suites):
        min_time = float('inf')
        min_index = 0
        for i, suite in enumerate(suites):
            if suite["time"] < min_time:
                min_time = suite["time"]
                min_index = i
        return min_index

    def make_suites(self, test_list, groups):
        suites = [{"list": [], "time": 0} for _ in range(groups)]
        round_robin = 0
        test_list.sort(key=lambda x: x[1] if x[1] is not None else float('inf'), reverse=True)
        for filename, time in test_list:
            if time is None:
                time = 0  # Assign a default time value if timing data is missing
            if time == 0:
                next_suite = round_robin
                round_robin = (round_robin + 1) % groups
            else:
                next_suite = self.smallest_group(suites)
            suites[next_suite]["list"].append(filename)
            suites[next_suite]["time"] += time
        return suites

class PhpUnitXmlManager:

    def generate_phpunit_xml(self, suites):
        phpunit_xml = ET.parse("phpunit.xml.dist")
        root = phpunit_xml.getroot()
        groups = len(suites)
        for i, suite in enumerate(suites):
            testsuite = ET.SubElement(root.find("testsuites"), "testsuite", {"name": f"split_group_{i}"})
            for filename in suite["list"]:
                if filename:
                    ET.SubElement(testsuite, "file").text = filename
        testsuite = ET.SubElement(root.find("testsuites"), "testsuite", {"name": f"split_group_{groups}"})
        ET.SubElement(testsuite, "file").text = "tests/phpunit/suites/ExtensionsParserTestSuite.php"
        xml_str = parseString(ET.tostring(root, encoding='utf-8')).toprettyxml()
        trimmed = '\n'.join([line for line in xml_str.split('\n') if line.strip()])
        with open("phpunit.xml", "w") as xml_file:
            xml_file.write(trimmed)

class Splitter:

    def split(self, xml_file, group_count, cache_file):
        timing_data = {}
        if cache_file is not None:
            print("Using results cache file", cache_file)
            timing_data = ResultCacheParser().parse_result_cache(cache_file)
        tests = TestListParser().parse_test_list(xml_file, timing_data)
        test_files = FilesystemScanner().find_test_files(tests)
        suites = SuiteBuilder().make_suites(test_files, group_count)
        PhpUnitXmlManager().generate_phpunit_xml(suites)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Supply a test xml file")
        sys.exit(1)

    if len(sys.argv) < 3:
        print("Specify a number of groups")
        sys.exit(1)

    cache_file = None
    if len(sys.argv) == 4:
        cache_file = sys.argv[3]

    Splitter().split(sys.argv[1], int(sys.argv[2]), cache_file)
