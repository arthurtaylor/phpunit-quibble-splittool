#!/bin/bash
set -ex
ROOT=`dirname $0`
${ROOT}/split_setup.sh
exec ${ROOT}/split_run_integration.sh
